package Practice1;

/**
 * Created by Vitaliy on 30.03.2016.
 */
public class Credit_Calculator {

    double termInMonth;
    double interestRate;
    double loanAmount;
    private double monthPayment;

    public Credit_Calculator(double loanAmount, double TermInMonth, double interestRate) {
        this.loanAmount = loanAmount;
        this.termInMonth = TermInMonth;
        this.interestRate = interestRate;
    }
    @Override
    public String toString() {
        return "Welcome to credit_Calculator: \n" +
                "Your loan Amount is: " + loanAmount +
                "\nTerms of your loan is: " + termInMonth +
                "\nInterest rate is: " + interestRate+"\n~~~~~~~~~~~~~~~~~~~~~~~";
    }


    public double calculateMonthlyPayment (){

        //converts interest rate to decimal number
        interestRate /=100.0;
        // Interest rate per month
        double interestRatePerMonth = interestRate/12.0;
        //calculate monthly payment
        double monthlyPayment = (loanAmount*interestRatePerMonth) /
                (1-Math.pow(1+interestRatePerMonth, -termInMonth));

        return monthPayment = monthlyPayment;

        }
    public void display(){
        double total = 0;
        for (int i = 1; i <= termInMonth; i++) {
            System.out.println("Payment for " +i+" month is " + String.format("%.2f", monthPayment));
            total+=monthPayment;

        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" +
                "Total sum of credit is: " + String.format("%.2f", total));

    }



    
   
}
