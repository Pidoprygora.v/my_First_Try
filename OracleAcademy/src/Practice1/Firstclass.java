package Practice1;

import javax.xml.bind.SchemaOutputResolver;

/**
 * Created by Vitaliy on 29.03.2016.
 */
public class Firstclass {

    static byte B;
    static short S;
    static int I;
    static long L;
    static float F;
    static double D;
    static char C;
    static boolean O;


    public static void main(String[] args) {
        //intialValues();
        //checkRectangularTriangle(1,1,5);
        //floatSamples();
        //loanAmount();
        //sumOfEvenNumbers();
        //sumOfPrimeNumbers();
        //checkNumbers(9,4,5);
        System.out.println(averageNumber(10000,34));

    }
    static void intialValues (){
        System.out.println("Byte value: "+B);
        System.out.println("Short value: "+S);
        System.out.println("Int value: "+I);
        System.out.println("Long value: "+L);
        System.out.println("Float value: "+F);
        System.out.println("Double value: "+D);
        System.out.println("Char value: "+C);
        System.out.println("Boolean value: "+O);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }
    static void floatSamples (){
        float a = 1.f; // 1. by default is double
        float b = 1; // 32 bit of int can fit in 32 bits of double
        float c = 0x1;
        float d = 0b1;
        float e = 1.0e1f; //by default is double
        float f = 01;
        System.out.println("Float 1. : " + a);
        System.out.println("Float 1 : " + b);
        System.out.println("Float 0x1 : " + c);
        System.out.println("Float 0b1 : " + d);
        System.out.println("Float 1.0e1 : " + e);
        System.out.println("Float 01 : " + f);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~");
    }

    static void shortSample(){
        short a = 12325+3424;
        short b = (short)(56+5.8); //cast operation required to initialized with loanAmount of double and int
        short c = (short)(5.6f+125);//cast operation required to initialized with loanAmount of float and int
        final byte x = 127;
        final short y = 32000;
        short d = x+y;
        short e = (short)(6.0f+34.567);
    }
    static void sum(){
        int c=0;
        for (int i=0; i<=20; i++) {
            c+=i;
        }
        System.out.println(c);
    }
    static void sumOfEvenNumbers(){
        int c=0;
        for (int i=0; i<=20; i++) {
            if (i%2==0) c+=i;
        }
        System.out.println(c);

    }
    static void sumOfPrimeNumbers(){
        int c=0;
        boolean b=true;
        m: for (int i = 2; i<=20;i++){
            for (int j =2; j<i; j++){
                if (i%j==0) continue m;
            }
            System.out.print(i+" ");
            c+=i;
        }
        System.out.println("\n"+"Sum of prime numbers: " + c);
    }
    static void checkNumbers(int a, int b, int c){
        if (a+b==c||a+c==b||b+c==a) {
            System.out.println(true);
        }
        else {
            System.out.println(false);
        }
    }


    public static void checkRectangularTriangle (int side1, int side2, int hipotenusa){
        System.out.println("Triangle Check with sides: "+"\n" + "side 1: " +side1 +"\n"+"side 2: " +side2 +"\n"+"hipotenusa: " +hipotenusa +"\n");
        if (side1+side2>hipotenusa)
            System.out.println((side1*side1+side2*side2==hipotenusa*hipotenusa? "It is a rectangular triangle": "It is not rectangular triangle"));
        else System.out.println("It is not a triangle");
        System.out.println("~~~~~~~~~~~~~~~~~~");
    }

    static int averageNumber (int a, int b){
        int count=0;
        int c =0;
        for (;b<=a;b++){
            count++;
            c +=b;
        }
        return c/count;
    }



}
