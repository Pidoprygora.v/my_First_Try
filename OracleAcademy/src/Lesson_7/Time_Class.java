package Lesson_7;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

/**
 * Created by Vitaliy on 26.03.2016.
 */
public class Time_Class {
    public static void main(String[] args) {

        LocalDateTime ldt = LocalDateTime.now();
        System.out.println(ldt);
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        String formatedtime = ldt.format(formatter);
        System.out.println(formatedtime);
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("EEEE dd MMMM yy", Locale.US);
        String formatedtime1 = ldt.format(formatter1);
        System.out.println(formatedtime1);
        
    }
}
