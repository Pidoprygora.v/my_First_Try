package ua.org.oa.pidoprygorav;

/**
 * Created by Vitaliy on 09.04.2016.
 *
 */
public class Buyer_statistic {
    private final int TARGET_AMOUNT = 495;
    private String name;
    private char sex;
    private byte age;
    private double totalSpent;
    private int numberOfVisits;
    private float discount;



    public Buyer_statistic(String name, char sex, int age, double totalSpent, int numberOfVisits) {
        setName(name);
        setSex(sex);
        setAge(age);
        setTotalSpent(totalSpent);
        setNumberOfVisits(numberOfVisits);
        setDiscount();
    }

    public String getName() {
        return name;
    }

    public char getSex() {
        return sex;
    }

    public byte getAge() {
        return age;
    }

    public double getTotalSpent() {
        return totalSpent;
    }

    public int getNumberOfVisits() {
        return numberOfVisits;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public void setAge(int age) {
        this.age = (byte)age;
    }
    public void setDiscount() {
        if (calcAverageRecipe()>= TARGET_AMOUNT) {
            this.discount = 0.05f;
        }
        else {
            this.discount = 0.0f;
        }
    }

    public void setTotalSpent(double totalSpent) {
        this.totalSpent = totalSpent;
    }

    public void setNumberOfVisits(int numberOfVisits) {
        this.numberOfVisits = numberOfVisits;
    }

    @Override
    public String toString() {
        return "Buyer information: \n" +
                "Buyer name: " + name + "\n" +
                "Buyer sex: " + sex + "\n"+
                "Buyer age: " + age + "\n"+
                "Buyer total spent per month: " + totalSpent + "\n"+
                "Buyer number of visits per month: " + numberOfVisits +"\n"+
                "Buyer earned a discount of: "+discount*100+"% for future purchases." ;
    }

    private double calcAverageRecipe() {
        double averageRecipe = totalSpent/numberOfVisits;
        return averageRecipe;
    }

    public void displayAverageRecipe() {
        System.out.println("Customer "+name+" has spent "+String.format("%.2f",calcAverageRecipe())+ " on average.");
    }
}
