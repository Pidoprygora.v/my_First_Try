package Lesson_8;


import com.sun.org.apache.xpath.internal.SourceTree;

/**
 * Created by Vitaliy on 26.03.2016.
 */
public class NewClass {

    public static void main(String[] args) {

        C c = new C();

    }
}
class A{
    public Field notStaticFieldA1 = new Field("notStaticFieldA1");
    public static Field StaticFieldA1 = new Field("StaticFieldA1");
    {
        System.out.println("Not static init block A");
    }
    static {
        System.out.println("Static init block A");
    }
    public A() {
        System.out.println("constructor A");
    }
    public Field notStaticFieldA2 = new Field("notStaticFieldA2");
    public static Field StaticFieldA2 = new Field("StaticFieldA2");

}

class B extends A{
    public Field notStaticFieldB1 = new Field("notStaticFieldB1");
    public static Field StaticFieldB1= new Field("StaticFieldB1");
    {
        System.out.println("Not static init block B");
    }
    static {
        System.out.println("Static init block B");
    }
    public B() {
        this (5);
        System.out.println("constructor B()");
    }
    public B(int i) {
        System.out.println("constructor B(int I)");
    }
    public Field notStaticFieldB2 = new Field("notStaticFieldB2");
    public static Field StaticFieldB2 = new Field("StaticFieldB2");


}

class C extends B {
    public Field notStaticFieldC1 = new Field("notStaticFieldC1");
    public static Field StaticFieldC1 = new Field("StaticFieldC1");
    {
        System.out.println("Not static init block C");
    }
    static {
        System.out.println("Static init block C");
    }
    public C() {
        System.out.println("constructor C");
    }
    public Field notStaticFieldC2 = new Field("notStaticFieldC2");
    public static Field StaticFieldC2 = new Field("StaticFieldC2");


}

class Field {
    public Field (String message){
        System.out.println(message);
    }
}